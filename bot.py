import json
import random
import sys
from mastodon import Mastodon

def main():
    try:
        with open('usercred.secret', 'r') as f:
            usercred = json.load(f)
    except:
        print("No usercred.secret file found")
        exit()
    mastodon = Mastodon(
        # replace values/files with your own
        client_id='clientcred.secret',
        access_token=usercred['token'],
        api_base_url=usercred['url']
    )
    with open("nom_sing", "r") as noun_file:
        noun_list = [line.rstrip() for line in noun_file]
    try:
        word = random.choice(noun_list)
    except:
        print("No word in the given list")
        sys.exit(2)

    toot(mastodon, word)

def toot(mastodon, word):
    mastodon.toot("On dit GNU/{}".format(word.capitalize()))

if __name__ == "__main__":
    main()
